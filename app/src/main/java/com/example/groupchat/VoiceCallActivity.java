package com.example.groupchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.groupchat.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.calling.CallListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class VoiceCallActivity extends AppCompatActivity {
    Intent intent;
    TextView username, type;
    CircleImageView btn_answer, btn_reject;
    String userid;
    FirebaseUser fuser;
    SinchClient sinchClient;
    Call call;
    DatabaseReference reference;
    String sender, receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_call);
        username = findViewById(R.id.username);
        type = findViewById(R.id.type);
        btn_answer = findViewById(R.id.btn_answer);
        btn_reject = findViewById(R.id.btn_reject);

        intent = getIntent();
        userid = intent.getStringExtra("userid");
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                receiver = user.getUsername();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                sender = user.getUsername();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        sinchClient = Sinch.getSinchClientBuilder()
                .context(this)
                .userId(fuser.getUid())
                .applicationKey("c2d6a59e-cb6c-442f-a687-39cb608ff321")
                .applicationSecret("6D2+a5Gs7ESPBgekpUFcEg==")
                .environmentHost("clientapi.sinch.com")
                .build();

        sinchClient.setSupportCalling(true);
        sinchClient.startListeningOnActiveConnection();
        sinchClient.start();

        sinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());

        CallUser();
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallProgressing(com.sinch.android.rtc.calling.Call call) {
            type.setText("Ringing...");
        }

        @Override
        public void onCallEstablished(com.sinch.android.rtc.calling.Call call) {
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            Toast.makeText(getApplicationContext(), "Call Established", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCallEnded(com.sinch.android.rtc.calling.Call endedCall) {
            Toast.makeText(getApplicationContext(), "Call Ended", Toast.LENGTH_LONG).show();
            call = null;
            SinchError a = endedCall.getDetails().getError();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
//            endedCall.hangup();
        }

        @Override
        public void onShouldSendPushNotification(com.sinch.android.rtc.calling.Call call, List<PushPair> list) {

        }
    }

    private class SinchCallClientListener implements CallClientListener {
        @Override
        public void onIncomingCall(CallClient callClient, final com.sinch.android.rtc.calling.Call incomingCall) {
            username.setText(sender);
            type.setText("Ringing...");

            call = incomingCall;
            call.answer();
            call.addCallListener(new SinchCallListener());
            Toast.makeText(getApplicationContext(), "Call is started", Toast.LENGTH_LONG).show();
            btn_answer.setVisibility(View.GONE);
//            btn_reject.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    call.hangup();
//                }
//            });

//            btn_answer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    call = incomingCall;
//                    call.answer();
//                    call.addCallListener(new SinchCallListener());
//                    Toast.makeText(getApplicationContext(), "Call is started", Toast.LENGTH_LONG).show();
//                }
//            });
        }
    }

    public void CallUser() {
        username.setText(receiver);
        type.setText("Calling...");
        btn_answer.setVisibility(View.GONE);
        if (call == null) {
            call = sinchClient.getCallClient().callUser(userid);
            call.addCallListener(new SinchCallListener());

        } else
            btn_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    call.hangup();
                }
            });
    }
}

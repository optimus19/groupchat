package com.example.groupchat.Fragments;

import com.example.groupchat.Notifications.MyResponse;
import com.example.groupchat.Notifications.Sender;

import retrofit2.Call;

import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAm7VXUNg:APA91bGutQv83UrZM2wkq0v8rp41VB68LB-oSkvvXLT9ZYWWxcGhsRkmv-cxTEePYM70BedR-H4mci50dqUXakkV7G14BLW_38CEjoWGLDI7hfNdJenIC0x1KBqWJh4TdVaTjS0yDwcL"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
